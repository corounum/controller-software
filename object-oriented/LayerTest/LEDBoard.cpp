#include "LEDBoard.h"
#include "Arduino.h"

/* CONSTRUCTOR
 *  
 *  LEDBoard()
 *  
 *  The constructor is called when we create a board layer.
 *  If we have any configuration to do for the board, this 
 *  is possibly the place to do it. 
 *  
 *  In this case, we set pin 13 as an output (to blink the LED),
 *  and we set pin 8 as an output (which, in theory, provides 
 *  power to a TMP36). 
 */
LEDBoard::LEDBoard() {
  pinMode(13, OUTPUT);
  pinMode(8, OUTPUT);
}


LEDBoard::~LEDBoard() {
  
}

/*
 * goToSleep() : none -> boolean
 * 
 * Takes in no parameters, and returns true if everything shut down
 * OK. 
 * 
 * Every board should have a goToSleep method. That way, we can always
 * tell every board to go into low-power mode. In our example here,
 * we turn off the two pins on our board that are doing stuff.
 * 
 * It's a simple example, but it illustrates the idea that this is
 * where we would issue commands to make sure the board is not consuming 
 * power unnecessarily.
 */
bool LEDBoard::goToSleep(){
  digitalWrite (13, LOW);
  digitalWrite (8, LOW);
  return true;
}

/*
 *  wakeUp() : none -> boolean
 *  
 *  Takes in no parameters, and returns true if everything 
 *  woke up OK.
 *  
 *  Before we can use one of the boards in the stack, we need to 
 *  tell it to wake up. That might involve sending commands over
 *  I2C to the board, or doing something else. In this case, we 
 *  are turning on the LED, and turning on the temperature sensor.
 */
bool LEDBoard::wakeUp(){
  digitalWrite (13, HIGH);
  digitalWrite (8, HIGH);
  return true;
}


/* 
 *  takeReading() : none -> integer
 *  
 *  This is an example of how we can add additional, board-specific
 *  functions to our class. Every board will not have a "takeReading"
 *  function... but, perahps every board that does sensing will.
 *  
 *  We might also have functions that do conversions. Or, who knows...
 *  we have to think more about what kind of programs we want to 
 *  write for our entire sensor stack.
 */
int LEDBoard :: takeReading(){
  return analogRead (A0); 
}

