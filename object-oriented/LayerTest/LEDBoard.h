#include "BoardLayer.h"

/* 
 *  CLASS DEFINITION
 *  
 *  We include the interface here, which defines
 *  the structure of our class. (A class, as you recall,
 *  is a template, or blueprint, for how we will design
 *  our code to represent things in the world.)
 *  
 *  Our class has a constructor and deconstructor, which
 *  are the first two functions. They *must* have the same
 *  name as the class itself. It's a rule.
 *  
 *  Then, we have our template functions. If we forget to
 *  include these, the code will not compile.
 *  
 *  Finally, we have any functions that are specific to this board.
 *  Someday, we might include an interface for sensor boards...
 *  it would then require that all sensor layers have a "takeReading"
 *  function, or... perhaps not. The point here is that interfaces
 *  help make sure we don't forget important things, and therefore
 *  it protects us from writing code that goes out into the world
 *  that does not work, or will fail in some mysterious way.
 */
class LEDBoard : public BoardLayer {
  public:
    // CONSTRUCTOR
    LEDBoard();
    // DECONSTRUCTOR
    ~LEDBoard();

    // INTERFACE FUNCTIONS
    bool goToSleep();
    bool wakeUp();

    // DOUBLE EXTRA BONUS FUNCTIONS
    int takeReading();
};

