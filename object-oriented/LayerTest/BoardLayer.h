/* 
 *  INTERFACE
 *  
 *  BoardLayer 
 *  
 *  This is an *interface*. That means it defines what functions
 *  must be present for every board layer. It's an odd syntax; 
 *  
 *  virtual __1__ ___2___(__3__) = 0;
 *  
 *  The first blank is the return type of the function. In this
 *  case, the two functions return booleans---true or false---to report
 *  if everything went OK when waking up and shutting down.
 *  
 *  The second blank is the name of the function.
 *  
 *  The third blank is where any parameters would go. We don't, at the 
 *  moment, pass any parameters to the sleep and wake functions.
 *  
 *  Everything else is C++ syntax. It's just that way.
 *  
 */
class BoardLayer{
  public:
    virtual bool goToSleep() = 0;
    virtual bool wakeUp() = 0;
};

