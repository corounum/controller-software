#include "LEDBoard.h"

// Create an LEDBoard. We're going to call it 
// "blinky." 
LEDBoard blinky;

// Set up the serial port, so we can print things
// to the console. Really only for testing.
void setup() {
  Serial.begin(9600);
}

void loop() {
  // First, we'll tell blinky to wake up. This 
  // turns out power to the LED and the TMP36.
  blinky.wakeUp();

  // We might have a short delay while all the board layers
  // are waking up. We would make a decision based on what they
  // layers are doing. A WiFi radio, for example, might require
  // 10-30 seconds to get a base station. Or, we might have a sensor
  // that has a warm-up time. There's other ways to do this.
  // For now, we'll wait one second to make sure the TMP36 is ready
  // to go.
  delay (1000);

  // Now, we ask blinky to take a reading. We store the result.
  int reading = blinky.takeReading();

  // We do some math to convert the raw reading to a voltage, and
  // then convert that voltage to a temperature in ˚F.
  float voltage = (reading * 5.0) / 1023.0;
  float temp = (voltage - 0.5) * 100.0;

  // Print what we came up with.
  Serial.println(temp);

  // Lastly, ask blinky to get ready to go into deep sleep.
  // Well, if this was real code, we'd be putting blinky into 
  // a low-power mode. In our example, it turns off the 
  // LED and TMP36.
  blinky.goToSleep();

  // Finally, we sleep for an hour before our next reading.
  // But, really, it's 3 seconds.
  delay (3000);


}
